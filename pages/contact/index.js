import { BsArrowRight } from 'react-icons/bs';
import { motion } from 'framer-motion';
import { fadeIn } from '../../variants';

const Contact = () => {
  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const data = Object.fromEntries(formData.entries());

    const response = await fetch('/api/sendmail', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });

    const result = await response.json();
    alert(result.message);
  };
 
  return (
    <div className='h-full'>
      <div className='container mx-auto py-32 text-center xl:text-left flex items-center justify-center h-full'>
        <div className='flex flex-col w-full max-w-[700px]'>
          <motion.h2 
            variants={fadeIn('up', 0.2)} 
            initial='hidden' 
            animate='show' 
            exit='hidden' 
            className='h2 text-gray-800 text-center mb-12'>
              Let&apos;s <span className='text-accent'>connect.</span>
          </motion.h2>
          <motion.form 
            onSubmit={handleSubmit}
            variants={fadeIn('up', 0.2)} 
            initial='hidden' 
            animate='show' 
            exit='hidden' 
            className='flex-1 flex flex-col gap-6 w-full mx-auto'>
            <div className='flex gap-x-6 w-full'>
              <input type='text' name='name' placeholder='Name' className='input text-gray-700 border-gray-400' required />
              <input type='email' name='email' placeholder='Your Email' className='input text-gray-700 border-gray-400' required />
            </div>
            <input type='text' name='subject' placeholder='Subject' className='input text-gray-700 border-gray-400' required />
            <textarea name='message' placeholder='Message' className='textarea text-gray-700  border-gray-400 file:text-justify' required></textarea>
            <button type='submit' className='btn text-gray-800 rounded-full border border-white/50 max-w-[170px]
              px-8 transition-all duration-300 flex items-center justify-center overflow-hidden 
              hover:border-accent group'>
                <span className='group-hover:-translate-y-[120%] group-hover:opacity-0 
                  transition-all duration-500'>
                  Let&apos;s talk
                </span>
                <BsArrowRight className='-translate-y-[120%] opacity-0 group-hover:flex 
                  group-hover:-translate-y-0 group-hover:opacity-100 transition-all duration-300 absolute text-[22px]'/>
              </button>
          </motion.form>
        </div>
      </div>
    </div>
  );
};

export default Contact;
