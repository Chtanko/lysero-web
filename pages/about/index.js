import React, { useState } from 'react';

// icons
import {
  FaHtml5,
  FaCss3,
  FaJs,
  FaReact,
  FaWordpress,
  FaFigma,
} from "react-icons/fa";

import {
  SiNextdotjs,
  SiFramer,
  SiAdobexd,
  SiAdobephotoshop,
} from "react-icons/si";

// data
const aboutData = [
  {
    title: 'skills',
    info: [
      {
        title: 'Web Development',
        icons: [
          <FaHtml5 key="FaHtml5" />,
          <FaCss3 key="FaCss3" />,
          <FaJs key="FaJs" />,
          <FaReact key="FaReact" />,
          <SiNextdotjs key="SiNextdotjs" />,
          <SiFramer key="SiFramer" />,
          <FaWordpress key="FaWordpress" />
        ],
      },
      {
        title: 'UI/UX Design',
        icons: [
          <FaFigma key="FaFigma" />,
          <SiAdobexd key="SiAdobexd" />,
          <SiAdobephotoshop key="SiAdobephotoshop" />,
        ],
      },
    ],
  },
  {
    title: 'credentials',
    info: [
      {
        title: 'Paris Blockchain Week',
        stage: '2024',
      },
      {
        title: 'Hackathon of Angers - WeForge',
        stage: '2022',
      },
      {
        title: 'Consultancy for Game design - AfunSoftware',
        stage: '2020',
      },
    ],
  },
];

//components
import Circles from '../../components/Circles';

//framer motion
import {motion} from 'framer-motion';
import {fadeIn} from '../../variants';

//counter
import CountUp from 'react-countup';

const About = () => {
  const [index, setIndex] = useState(0);

  return (
    <div className='h-full'>
      <div className='w-full h-full bg-gradient-to-r from-white via-gray-100 to-gray-200 py-32 text-center xl:text-left'>
        <Circles />
        <div className='container mx-auto h-full flex flex-col items-center xl:flex-row gap-x-6'>
          { /* text */ }
          <div className='flex-1 flex flex-col justify-center'>
            <div>
              <motion.h2 
                variants={fadeIn('right', 0.2)} 
                initial='hidden' 
                animate='show' 
                exit='hidden'
                className='h2 text-gray-800'
              >
                Our amazing <span className='text-accent'>achievements</span>
              </motion.h2>
              <motion.p 
                variants={fadeIn('right', 0.4)} 
                initial='hidden' 
                animate='show' 
                exit='hidden'
                className='max-w-[500px] mx-auto xl:mx-0 mb-6 xl:mb-12 px-2 xl:px-0 text-gray-700 text-justify'
              >
                At Lysero, we specialize in top-tier software solutions tailored to your needs. 
                Our dedicated team, advanced technology, and deep industry insight ensure 
                innovative results. We prioritize understanding your objectives to exceed expectations.
              </motion.p >
              { /* counters */ }
              <div className='hidden md:flex md:max-w-xl xl:max-w-none mx-auto xl:mx-0 mb-8'>
                <div className='flex flex-1 xl:gap-x-6'>
                  { /* experience */ }
                  <motion.div 
                    variants={fadeIn('right', 0.6)} 
                    initial='hidden' 
                    animate='show' 
                    exit='hidden'
                    className='relative flex-1 after:w-[1px] after:h-full after:bg-gray-300 ater:absolute after:top-0 after:right-0'>
                    <div className='text-2xl xl:text-4x; font-extrabold text-accent mb-2'>
                      <CountUp start={0} end={3} duration={5} /> + 
                    </div>
                    <div className='text-xs uppercase tracking-[1px] leading-[1.4] max-w-100px text-gray-600'>
                      Years of business
                    </div>
                  </motion.div>
                  { /* projects */ }
                  <div className='relative flex-1 after:w-[1px] after:h-full after:bg-gray-300 ater:absolute after:top-0 after:right-0'>
                    <div className='text-2xl xl:text-4x; font-extrabold text-accent mb-2'>
                      <CountUp start={0} end={350} duration={5} /> + 
                    </div>
                    <div className='text-xs uppercase tracking-[1px] leading-[1.4] max-w-100px text-gray-600'>
                      Finished projects
                    </div>
                  </div>
                  { /* countries */ }
                  <div className='relative flex-1 after:w-[1px] after:h-full after:bg-gray-300 ater:absolute after:top-0 after:right-0'>
                    <div className='text-2xl xl:text-4x; font-extrabold text-accent mb-2'>
                      <CountUp start={0} end={3} duration={5} />
                    </div>
                    <div className='text-xs uppercase tracking-[1px] leading-[1.4] max-w-100px text-gray-600'>
                      Countries we worked with
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* info */}
          <motion.div 
            variants={fadeIn('left', 0.4)} 
            initial='hidden' 
            animate='show' 
            exit='hidden'
            className='flex flex-col w-full xl:max-w-[48%] h-[480px]'
          >
            <div className='flex gap-x-4 xl:gap-x-8 mx-auto xl:mx-0 mb-4 text-gray-800'>
              {aboutData.map((item, itemIndex) => {
                return (
                  <div 
                    key={itemIndex} 
                    className={`${
                      index === itemIndex && 
                    'text-accent after:w-[100%] after:bg-accent after:transition-all after:duration-300'
                    } cursor-pointer capitalize xl:text-lg relative after:w-8 after:h-[2px] after:bg-gray-800 after:absolute after:-bottom-1 after:left-0`}
                    onClick={() => setIndex(itemIndex)}
                  >
                    {item.title}
                  </div>
                );
              })}
            </div>
            <div className='py-2 xl:py-6 flex flex-col gap-y-2 xl:gap-y-4 items-center xl:items-start text-gray-700'>
              {aboutData[index].info.map((item, itemIndex)=> {
                return (
                  <div 
                    key={itemIndex} 
                    className='flex-1 flex flex-col md:flex-row max-w-max gap-x-2 items-center text-gray-700 text-justify'
                  >
                    {/* title */}
                    <div className='font-light mb-2 md:mb-0'>{item.title}</div>
                    <div className='hidden md:flex'>-</div>
                    <div>{item.stage}</div>
                    <div className='flex gap-x-4'>
                    {/* icons */}
                      {item.icons?.map((icon, iconIndex) => {
                        return <div key={iconIndex} className='text-2xl text-gray-800'>{icon}</div>
                      })}
                    </div>
                  </div>
                );
              })}
            </div>
          </motion.div>
        </div>
      </div>
    </div>
  );
};

export default About;
