import nodemailer from 'nodemailer';

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: parseInt(process.env.SMTP_PORT),
      secure: process.env.SMTP_SECURE === 'true', // Assurez-vous que ce soit bien un booléen
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS,
      },
    });

    const { name, email, subject, message } = req.body;

    try {
      await transporter.sendMail({
        from: process.env.SMTP_USER, // Votre adresse WorkMail
        to: "contact@lysero.com",  // Changez cette adresse si nécessaire
        subject: `Message de ${name} - ${subject}`,
        text: `Nom : ${name}\nEmail : ${email}\nMessage : ${message}`,
        html: `<b>Nom :</b> ${name}<br><b>Email :</b> ${email}<br><b>Message :</b> ${message}`,
      });

      res.status(200).json({ success: true, message: 'Email envoyé avec succès' });
    } catch (error) {
      console.error('Erreur lors de l\'envoi de l\'email:', error);
      res.status(500).json({ success: false, message: 'Échec de l\'envoi de l\'email', error: error.message });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    res.status(405).end(`Méthode ${req.method} non autorisée`);
  }
}
