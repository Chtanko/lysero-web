//components
import ServiceSlider from '../../components/ServiceSlider';
import Bulb from '../../components/Bulb';
import Circles from '../../components/Circles';

//framer motion
import { motion } from 'framer-motion';
import { fadeIn } from '../../variants';

const Services = () => {
  return (
    <div className='w-full h-full bg-gradient-to-r from-white via-gray-100 to-gray-200 py-32 text-center xl:text-left'>
    <Circles />
          <div className='container mx-auto'>
            <div className='flex flex-col xl:flex-row gap-x-8'>
            { /* text */ }
              <div className='text-center flex xl:w-[30vw] flex-col lg:text-left mb-4 
              xl:mb-0'>
                <motion.h2 
                  variants={fadeIn('up', 0.3)} 
                  initial='hidden'
                  animate='show'
                  exit='hidden'
                  className='h2 xl:mt-8'
                >
                  Our Services <span className='text-accent'>.</span>
                </motion.h2>
                <motion.p 
                  variants={fadeIn('up', 0.4)} 
                  initial='hidden'
                  animate='show'
                  exit='hidden'
                  className='mb-4 max-w-[400px] mx-auto lg:mx-0'>
                  Lysero is a leading tech innovator, specializing in advanced software development, 
                  web3 solutions, and crypto mining hardware. They deliver 
                  tailored software, harness the power of blockchain, and offer efficient crypto mining solutions,
                  positioning themselves as a pivotal force in the technological landscape.
                </motion.p>
              </div>
              { /* slider */ }
              <motion.div 
                variants={fadeIn('down', 0.6)} 
                initial='hidden'
                animate='show'
                exit='hidden'
                className='w-full xl:max-w-[65%]'
              >
                <ServiceSlider />
              </motion.div>
            </div>
        </div>
        <Bulb />
      </div>
  );
};

export default Services;
