# Étape de construction
FROM node:16 AS builder
WORKDIR /app
COPY package*.json ./
RUN npm install
RUN npm install sharp
COPY . .
RUN npm run build

# Étape d'exécution
FROM node:16-alpine
WORKDIR /app
COPY --from=builder /app ./

# Installation de la CLI AWS (si nécessaire)
RUN apk --no-cache add \
    python3 \
    py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install awscli

# Exposer le port 3000
EXPOSE 3000

# Utiliser la commande de démarrage pour Next.js en production
CMD ["npm", "start"]
