// Socials.js
import Link from 'next/link';
import { RiLinkedinLine } from 'react-icons/ri';

const Socials = () => {
  return (
    <div className='flex items-center gap-x-5 text-lg fixed bottom-5 right-5 md:static md:bottom-auto md:right-auto'>
      <Link href={'https://www.linkedin.com/company/lysero/?viewAsMember=true'} className='hover:text-accent transition-all duration-300'>
        <RiLinkedinLine className='text-gray-600 hover:text-gray-800 transition-colors duration-300' />
      </Link>
    </div>
  );
};

export default Socials;
