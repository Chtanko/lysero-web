// icons
import { RxPencil2, RxDesktop, RxRocket, RxArrowTopRight, RxComponentInstance } from "react-icons/rx";

const serviceData = [
  {
    icon: <RxPencil2 />,
    title: 'Blockchain',
    description: 'Harness the power of decentralized technologies',
  },
  {
    icon: <RxDesktop />,
    title: 'Software',
    description: 'Our team delivers advanced software solutions',
  },
  {
    icon: <RxRocket />,
    title: 'Play to Earn',
    description: 'Develop original blockhain gaming projects',
  },
  {
    icon: <RxComponentInstance />,
    title: 'Mining Hardware',
    description: 'Provides crypto bespoke mining hardware',
  },
];

//import required modules
import { FreeMode, Pagination } from 'swiper';

//import swiper react components
import { Swiper, SwiperSlide } from 'swiper/react';

//import swiper styles
import 'swiper/css';
import 'swiper/css/free-mode';
import 'swiper/css/pagination';

const ServiceSlider = () => {
  return <Swiper breakpoints={{
    320: {
      slidesPerView: 1,
      spaceBetween: 15
    },
    640: {
      slidesPerView: 3,
      spaceBetween: 15
    }
  }}
  freeMode={true}
  pagination={{
    clickable: true 
  }}
  modules={[FreeMode, Pagination]}
  className='h-[240px] sm:h-[340px]'
  >
    {
      serviceData.map((item,index)=> {
        return <SwiperSlide key={index}>
          <div className='bg-[rgba(65,47,123,0.15)] h-max rounded-lg px-6 py-8 flex sm:flex-col 
          gap-x-6 sm:gap-x-0 group cursor-pointer hover:bg-[rgba(89,65,169, 0.15) transition-all duration-300]'>
            { /* icon */ }
            <div className='text-4xl text-accent mb-4'>{item.icon}</div>
            { /* title & desc */ }
            <div className='mb-8'>
              <div className='mb-2 text-lg'>{item.title}</div>
              <p className='max-w-[350px] leading-normal'>
                {item.description}
              </p>
            </div>

          </div>
        </SwiperSlide>
      })}
  </Swiper>;
};

export default ServiceSlider;