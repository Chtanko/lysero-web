import Image from 'next/image';
import Link from 'next/link';
import Socials from '../components/Socials';

const Header = () => {
  return (
    <header className='absolute z-30 w-full flex items-center px-16 xl:px-0 xl:h-[90px]'>
      <div className='container mx-auto'>
        
        <div className='flex flex-col lg:flex-row justify-between items-center gap-y-6 py-8'>
          <div className='relative' style={{ width: '100%', maxWidth: '300px', height: '150px' }}> {/* Ajustez ici la hauteur à la valeur exacte nécessaire */}
            <Link href='/' legacyBehavior>
              <a>
                <Image
                  src='/lysero.png'
                  alt='Software Hardware Web3'
                  priority={true}
                  layout='fill'
                  objectFit='contain'
                />
              </a>
            </Link>
          </div>
          <Socials />
        </div>
      </div>
    </header>
  );
};

export default Header;
