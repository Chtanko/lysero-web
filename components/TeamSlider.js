// testimonial data
const teamSlider = [
  {
    image: '/t-avatar-1.png',
    name: 'Constantin Chtanko',
    position: 'Chief Executive Officer - CEO',
    message:
      'As the CEO of Lysero, Constantin Chtanko leads the company with a clear vision and strategic direction. His role is pivotal in driving the company\'s growth and success in the digital and IT sector. Constantin oversees all aspects of the business, from software development to IT consulting, ensuring that Lysero remains at the forefront of innovation and technological advancement. With a focus on delivering exceptional solutions to clients, Constantin guides the team in achieving excellence in every project. His leadership style fosters collaboration, creativity, and a commitment to exceeding client expectations, making Lysero a trusted partner in the realm of digital transformation.',
  },
  {
    image: '/t-avatar-2.png',
    name: 'Nikita Cherniavskii',
    position: 'Director',
    message:
      'As the Director General of Lysero, Nikita Cherniavskii plays a crucial role in managing the company\'s day-to-day operations and strategic initiatives. He works closely with CEO Constantin Chtanko to ensure that all projects align with the company’s vision and market demands. Nikita is instrumental in streamlining operations, enhancing productivity, and driving the development of innovative IT solutions. His leadership is key in fostering a culture of technical excellence and operational efficiency, ensuring that Lysero not only meets but exceeds client expectations in the fast-evolving tech landscape.',
  },
  {
    image: '/t-avatar-3.png',
    name: 'Nikolaï Belenskii',
    position: 'Chief Technical Officer - CTO',
    message:
      'As the Chief Technical Officer (CTO) of Lysero, Nikolaï Belenski is responsible for overseeing the development. He leads the engineering teams, sets the technical vision, and drives innovation across the company\'s product lines. Nikolaï\'s role is critical in ensuring that Lysero stays ahead of technological trends, integrating advanced systems and solutions that enhance service delivery and client satisfaction. His expertise is vital in transforming complex systems into user-friendly solutions that support Lysero\'s goals of efficiency and growth in the IT sector.',
  },
];

// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';


// Import required modules
import { Navigation, Pagination } from 'swiper';

// Icons
import { FaQuoteLeft } from 'react-icons/fa';

// Next Image
import Image from 'next/image';

const TeamSlider = () => {
  return (
    <Swiper 
      navigation={true}
      pagination={{
        clickable: true,
      }}
      modules={[Navigation, Pagination]}
      className='h-[480px]'
    >
      {teamSlider.map((person, index) => {
        return (
          <SwiperSlide key={index}>
            <div className='flex flex-col items-center md:flex-row gap-x-8 h-full px-16'>
              {/* avatar, name, position */}
              <div className='flex flex-col justify-center text-center'>
                <div className='w-full max-w-[300px] flex flex-col xl:justify-center 
                  items-center relative mx-auto xl:mx-0'>
                  {/* avatar */}
                  <div className='mb-2 mx-auto'>
                    <Image 
                      src={person.image} 
                      width={400} 
                      height={400} 
                      alt='' 
                    />
                  </div>
                  {/* name */}
                  <div className='text-lg text-gray-800'>{person.name}</div>
                  {/* position */}
                  <div className='text-[12px] uppercase font-extralight tracking-widest text-gray-500'>{person.position}</div>
                </div>
              </div>
              {/* quote & message */}
              <div className='flex-1 flex flex-col justify-center before:w-[1px] 
                xl:before:bg-white/20 xl:before:absolute xl:before:left-0 xl:before:h-[200px] relative xl:pl-20'>
                {/* quote icon */}
                <div className='mb-4'>
                  <FaQuoteLeft className='text-4xl xl:text-6xl text-gray-400 mx-auto md:mx-0' />
                </div>
                {/* message */}
                <div className='xl:text-lg text-gray-700 text-justify md:text-left leading-relaxed'>
                  {person.message}
                </div>
              </div>
            </div>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default TeamSlider;
