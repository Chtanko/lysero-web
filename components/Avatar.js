//next image
import Image from 'next/image';
import Tilt from 'react-parallax-tilt';

const Avatar = () => {
  return ( 
<Tilt>
<div className='hidden xl:flex xl:max-w-done' 
  style={{ marginTop: '-20px', width: '650px', height: '650px', position: 'relative', left: '80px' }}>
    <Image 
      src={'/lion-icon.png'} 
      priority={true}
      fill
      style={{ objectFit: 'contain', transform: 'translateY(-5%)' }}
      alt='' 
      className='translate-z-0 w-full h-full'
    />
  </div>
</Tilt>


  );
};

export default Avatar;
