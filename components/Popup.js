// Popup.js
import React from 'react';

function Popup({ isOpen, message, onClose }) {
  if (!isOpen) return null;

  return (
    <div className="popup-overlay">
      <div className="popup">
        <h2>{message}</h2>
        <button onClick={onClose}>Close</button>
      </div>
      {/* Styles ici ou dans un fichier CSS externe */}
      <style jsx>{`
        .popup-overlay {
          position: fixed;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
          background-color: rgba(0,0,0,0.5);
          display: flex;
          justify-content: center;
          align-items: center;
        }
        .popup {
          background: white;
          padding: 20px;
          border-radius: 8px;
          box-shadow: 0 2px 12px rgba(0,0,0,0.4);
        }
      `}</style>
    </div>
  );
}

export default Popup; // Assurez-vous que cette ligne est correcte
